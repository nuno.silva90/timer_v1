#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "FS.h"
#include <Ticker.h>
#include <ArduinoOTA.h>

#define TESTFILE "/testfile.txt"
#define TIME1FILE "/time1.txt"
#define TIME2FILE "/time2.txt"
#define TIME3FILE "/time3.txt"
#define LASTTIMEFILE "/lasttime.txt"

///////////////////////////////////////////////////////
//Config AP name | AP password | OTA name | OTA pass //
///////////////////////////////////////////////////////

//AP name
#define APSSID      "NodeMCU"

//AP password             
#define APPSK       "12345678"

//OTA name            
#define OTANAME     "ota_NodeMCU"

//OTA pass -> to enable, uncomment line 146 too             
//#define OTAPASS   "admin"               


//TODO: update functions
void update_timer_1s();
void update_timer_10ms();
void timer_testing_outputs(); 
void update_adc_10ms();

//TODO: quando se introduz os valores dos timers, nao mudar de pagina ou voltar automaticamente passado 3 seg
//TODO: aux_save fica a 1 depois de gravar os segundos mas nao volta a 0, implementer timer de 1 min para voltar a colocar a 0
//TODO: no teste de saidas, abrir pagina nova e depois return to home ao fim dos segundos do teste

//Timers
Ticker timer1;
Ticker timer2;
Ticker timer3;
Ticker timer4;
Ticker timer5;
Ticker timer6;

//Web server config
AsyncWebServer server(80);
const char* ssid = APSSID;
const char* password = APPSK;

//Adc
const int analogInPin = A0;
float inputVoltage = 0;

//Outputs
uint8_t output0pin = 4; //0
bool output0State = LOW;
String output0Status;
uint8_t output2pin = 5; //2
bool output2State = LOW;
String output2Status;
uint8_t output4pin = 2; //4
bool output4State = LOW;

//Web server variables
const char* PARAM_INPUT_1 = "time1";
const char* PARAM_INPUT_2 = "time2";
const char* PARAM_INPUT_3 = "time3";

//time variables
unsigned int time1 = 10;
unsigned int time2 = 11;
unsigned int time3 = 12; //pause
double seconds = 0; //seconds counter
unsigned int testing_time = 30; //time to test the outputs
unsigned int time_to_save = 60; //waiting time to be possible to save again the seconds in the file 
unsigned int cool_down_time = 5; 
int aux_save = 0;

char str[10];

bool spiffsActive = false;

bool testing_outputs = 0;
bool test_output0_on = 0;
bool test_output0_off= 0;
bool test_output2_on = 0;
bool test_output2_off= 0;
bool cool_down_enabled = 0;


void setup(){
  pinMode(output0pin, OUTPUT);
  pinMode(output2pin, OUTPUT);
  pinMode(output4pin, OUTPUT);
  digitalWrite(output0pin, LOW);
  digitalWrite(output2pin, LOW);

  Serial.begin(115200);
  delay(1000);
  
  WiFi.softAP(ssid, password);
  Serial.println(WiFi.localIP());
    
  // SPIFFS Init
  if (SPIFFS.begin()) {
      Serial.println("SPIFFS Active");
      Serial.println();
      spiffsActive = true;
  } else {
      Serial.println("Unable to activate SPIFFS");
  }
  delay(2000);   
  
  //Read files from flash
  time1 = readFile(SPIFFS, TIME1FILE).toInt();
  Serial.print("Time 1: ");
  Serial.println(time1);
  
  time2 = readFile(SPIFFS, TIME2FILE).toInt();
  Serial.print("Time 2: ");
  Serial.println(time2);
  
  time3 = readFile(SPIFFS, TIME3FILE).toInt();
  Serial.print("Time 3: ");
  Serial.println(time3);
  
  seconds = readFile(SPIFFS, LASTTIMEFILE).toDouble();
  Serial.print("Last time: ");
  Serial.println(seconds);
  
  
  //Web server init
  web_server_startup();
  
  //Timers init
  timer1.attach(1, update_timer_1s);
  timer2.attach(0.01, update_timer_10ms); 
  timer5.attach(0.01, update_adc_10ms);


   //OTA section
  
  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname(OTANAME);

  // No authentication by default
  //ArduinoOTA.setPassword(OTAPASS);
   
  //OTA state
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }

  // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
  Serial.println("Start updating " + type);
  });

  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });

  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  
  
  // initialize OTA capability
  ArduinoOTA.begin();

}


void loop(){ 
	// handle any OTA requests
  ArduinoOTA.handle();
  
  // normal sketch code
	if (testing_outputs == 0)
	{
		if(seconds<=(time3))
		{
		digitalWrite(output0pin, LOW);
		digitalWrite(output2pin, LOW);	
		}
		else if(seconds<=(time3+time1))
		{
		digitalWrite(output0pin, HIGH);
		digitalWrite(output2pin, LOW);		
		}	
		else if(seconds<=(time3+time1+time3))
		{
		digitalWrite(output0pin, LOW);
		digitalWrite(output2pin, LOW);
		}
		else if(seconds<=(time3+time1+time3+time2))
		{
		digitalWrite(output0pin, HIGH);
		digitalWrite(output2pin, HIGH);
		}
		if(seconds>(time3+time1+time3+time2))
			seconds=0;
	}
	else 
	{		
		if(test_output0_on == 1 && test_output0_off == 0)
		{
			digitalWrite(output0pin, HIGH);			
		}
		else if(test_output0_on == 0 && test_output0_off == 1)
		{
			digitalWrite(output0pin, LOW);			
		}
		
		if(test_output2_on == 1 && test_output2_off == 0)
		{
			digitalWrite(output2pin, HIGH);			
		}
		else if(test_output2_on == 0 && test_output2_off == 1)
		{
			digitalWrite(output2pin, LOW);			
		}
		
	}
}

//timer 1
void update_timer_1s() {
  digitalWrite(output4pin, output4State);
  output4State = !output4State;

  Serial.print(seconds); Serial.print(" ");  
  inputVoltage = analogRead(analogInPin)*(3.3/1024);  
  Serial.print(inputVoltage); Serial. print(" "); Serial.println(analogRead(analogInPin));
 
}

//timer 2
void update_timer_10ms() {
	//update seconds
	seconds += 0.01;  
}

//timer 5
void update_adc_10ms(){
	inputVoltage = analogRead(analogInPin)*(3.3/1024); 
	if ((inputVoltage < 1.7) && (aux_save == 0)){
		sprintf(str, "%d", int(seconds));
		writeFile(SPIFFS, LASTTIMEFILE, str);
		aux_save = 1;
		timer4.attach(time_to_save, save_again);	
  }

}

//timer 4
void save_again(){
	Serial.print('Possible to save again...');
	aux_save = 0;
	timer4.detach();
}

//timer 3
void timer_testing_outputs(){
	Serial.println("Entrou no timer testing outputs...");
	if(testing_outputs == 1){
		Serial.println("disabling output timer...");		
		timer2.attach(0.01, update_timer_10ms);		
		testing_outputs = 0;	
		test_output0_on = 0;
		test_output0_off = 1;
		test_output2_on = 0;
		test_output2_off = 1;
	}
	timer3.detach();
	
}

//timer6
void timer_cool_down(){
	Serial.println("trigger cool down timer...");
	cool_down_enabled = 0;
	timer6.detach();
}

void web_server_startup(){	
	
	server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
		request->send(SPIFFS, "/index.html", String(), false, processor);
	});  
	server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
		request->send(SPIFFS, "/style.css", "text/css");
	});  
	  
	  
	server.on("/get", HTTP_GET, [] (AsyncWebServerRequest *request) {	
		String inputMessage;
		String inputParam;
		//unsigned int aux = 0;
			
		// GET input1 value on <ESP_IP>/get?input1=<inputMessage>	
		if (request->hasParam(PARAM_INPUT_1)) {
		  Serial.println("Entrou param_input_1...");
		  inputMessage = request->getParam(PARAM_INPUT_1)->value();
		  inputParam = PARAM_INPUT_1;
		  Serial.print(inputParam); Serial.println(inputMessage);
		  writeFile(SPIFFS, TIME1FILE, inputMessage.c_str());
		  time1 = inputMessage.toInt();		  
		} 
		else if (request->hasParam(PARAM_INPUT_2)) {
		  Serial.println("Entrou param_input_2...");
		  inputMessage = request->getParam(PARAM_INPUT_2)->value();
		  inputParam = PARAM_INPUT_2;
		  Serial.print(inputParam); Serial.println(inputMessage);
		  writeFile(SPIFFS, TIME2FILE, inputMessage.c_str());
		  time2 = inputMessage.toInt();
		} 
		else if (request->hasParam(PARAM_INPUT_3)) {
		  Serial.println("Entrou param_input_3...");
		  inputMessage = request->getParam(PARAM_INPUT_3)->value();
		  inputParam = PARAM_INPUT_3;
		  Serial.print(inputParam); Serial.println(inputMessage);
		  writeFile(SPIFFS, TIME3FILE, inputMessage.c_str());
		  time3 = inputMessage.toInt();
		} 	
		else {
		  inputMessage = "No message sent";
		}
		delay(250);	
		request->redirect("/");

		
//		request->send(SPIFFS, "/index.html", String(), false, processor);

    });  
  
	//Buttons to test outputs
	server.on("/on0", HTTP_GET, [](AsyncWebServerRequest *request){	
	if(cool_down_enabled==0){
		testing_outputs = 1;
		test_output0_on = 1;
		test_output0_off = 0;
		test_output2_on = 0;
		test_output2_off = 1;	
		timer2.detach();
		timer3.attach(testing_time, timer_testing_outputs);
		cool_down_enabled = 1;
		timer6.attach(cool_down_time, timer_cool_down);
	}	
	request->send(SPIFFS, "/index.html", String(),false, processor);	
	}); 

	server.on("/off0", HTTP_GET, [](AsyncWebServerRequest *request){
	testing_outputs = 0;	
	test_output0_on = 0;
	test_output0_off = 1;	
	timer2.attach(0.01, update_timer_10ms);	
	request->send(SPIFFS, "/index.html", String(),false, processor);
	});

	server.on("/on2", HTTP_GET, [](AsyncWebServerRequest *request){	
	if(cool_down_enabled==0){
		testing_outputs = 1;
		test_output0_on = 1;
		test_output2_on = 1;
		test_output2_off = 0;
		test_output0_off = 0;	
		timer2.detach();
		timer3.attach(testing_time, timer_testing_outputs);
		cool_down_enabled = 1;	
		timer6.attach(cool_down_time, timer_cool_down);
	}
	request->send(SPIFFS, "/index.html", String(),false, processor);
	});  

	server.on("/off2", HTTP_GET, [](AsyncWebServerRequest *request){
	testing_outputs = 0;	
	test_output2_on = 0;
	test_output0_on = 0;
	test_output2_off = 1;
	test_output0_off = 1;
	timer2.attach(0.01, update_timer_10ms);		
	request->send(SPIFFS, "/index.html", String(),false, processor);
	}); 	
	
	server.on("/seconds", HTTP_GET, [](AsyncWebServerRequest *request){	
	sprintf(str, "%d", int(seconds));
	//Serial.print(str);
	request->send(200, "text/plain", str);
	}); 
	
	server.on("/output0", HTTP_GET, [](AsyncWebServerRequest *request){	
	if(digitalRead(output0pin)){
		  output0Status = "ON";
		}
		else{
		  output0Status = "OFF";
		}
	//Serial.print(output0Status);	
	request->send(200, "text/plain", output0Status);
	}); 
	
	server.on("/output2", HTTP_GET, [](AsyncWebServerRequest *request){	
	if(digitalRead(output2pin)){
		  output2Status = "ON";
		}
		else{
		  output2Status = "OFF";
		}
	//Serial.println(output2Status);	
	request->send(200, "text/plain", output2Status);
	}); 
	
	server.onNotFound(notFound);  
	server.begin();	
}


String processor(const String& var){
  Serial.print(var); Serial.print(" ");
  
  if(var == "SECONDS"){
	   Serial.println(seconds);	   
	   sprintf(str, "%d", seconds);
	   return str;
	}
  
   if(var == "TIME1"){
	   Serial.println(time1);	   
	   sprintf(str, "%d", time1);
	   return str;
	}
	if(var == "TIME2"){
	   Serial.println(time2);	   
	   sprintf(str, "%d", time2);
	   return str;
	}
	if(var == "TIME3"){
	   Serial.println(time3);	   
	   sprintf(str, "%d", time3);
	   return str;
	}
	
   if(var == "STATE"){
		if(digitalRead(output0pin)){
		  output0Status = "ON";
		}
		else{
		  output0Status = "OFF";
		}
		Serial.println(output0Status);
		return output0Status;
	}	
	if(var == "STATE2"){
		if(digitalRead(output2pin)){
		  output2Status = "ON";
		}
		else{
		  output2Status = "OFF";
		}
		Serial.println(output2Status);
		return output2Status;
	}
  
}

void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

String readFile(fs::FS &fs, const char * path){
  Serial.printf("Reading file: %s\r\n", path);
  File file = fs.open(path, "r");
  if(!file || file.isDirectory()){
    Serial.println("- empty file or failed to open file");
    return String();
  }
  // Serial.println("- read from file:");
  String fileContent;
  while(file.available()){
    fileContent+=String((char)file.read());
  }
  // Serial.println(fileContent);
  return fileContent;
}

void writeFile(fs::FS &fs, const char * path, const char * message){
  Serial.printf("Writing file: %s\r\n", path);
  File file = fs.open(path, "w");
  if(!file){
    Serial.println("- failed to open file for writing");
    return;
  }
  if(file.print(message)){
    Serial.println("- file written");
  } else {
    Serial.println("- write failed");
  }
}
